import axios from "@/plugins/axios";

export const getAllNews = async () => {
  const response = await axios.get(`/news`, {
    params: { populate: "image" },
  });
  return response.data.data;
};

export const getNewsById = async (id) => {
  const response = await axios.get(`/news/${id}`, {
    params: { populate: "image" },
  });
  return response.data.data;
};
