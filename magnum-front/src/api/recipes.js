import axios from "@/plugins/axios";

const apiClient = axios.create({
    baseURL: 'http://135.181.105.154:1337/api/',
    withCredentials: false,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })

  export default {
    getRecipes() {
      return apiClient.get('/recipes')
    },
  
    getRecipe(id) {
      console.log(id)
      return apiClient.get(`/recipes/${id}`)
    },
  }