import axios from "@/plugins/axios";
import qs from "qs";

export const getAllProducts = async (page = 1, pageSize = 100) => {
  const query = qs.stringify(
    {
      pagination: {
        page: page,
        pageSize: pageSize,
      },
      populate: ["image"],
    },
    {
      encodeValuesOnly: true,
    }
  );
  const response = await axios.get(`/products?${query}`);
  return response?.data?.data;
};
export const getProductById = async (id) => {
  const query = qs.stringify(
    {
      populate: "*",
    },
    {
      encodeValuesOnly: true,
    }
  );
  const response = await axios.get(`/products/${id}?${query}`);
  return response?.data?.data;
};
