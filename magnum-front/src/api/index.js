import axios from "@/plugins/axios";
import qs from "qs";

export const getAllCities = async (sortType = "asc") => {
  const query = qs.stringify(
    {
      sort: [`name:${sortType}`],
    },
    {
      encodeValuesOnly: true,
    }
  );
  const response = await axios.get(`/cities?${query}`);
  return response?.data?.data;
};

export const getShopsByCityId = async (id) => {
  const response = await axios.get(
    `/cities/${id}?populate[shops][populate][shop_type][populate][0]=logo`
  );
  return response?.data.data?.attributes?.shops?.data;
};

export const getBanners = async () => {
  const response = await axios.get(`/banners?populate=image_mob,image`);
  return response?.data?.data;
};
export const getPromos = async (page = 1, pageSize = 100) => {
  const query = qs.stringify(
    {
      pagination: {
        page: page,
        pageSize: pageSize,
      },
      populate: "*",
    },
    {
      encodeValuesOnly: true,
    }
  );
  const response = await axios.get(`/promos?${query}`);
  return response?.data;
};
