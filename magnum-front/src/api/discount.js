import axios from "@/plugins/axios";
import qs from "qs";

export const getAllDiscounts = async () => {
  const response = await axios.get(`/discounts`);
  return response?.data?.data;
};

export const getAllProductsByDiscountId = async (id) => {
  const query = qs.stringify(
    {
      populate: { products: { populate: ["image"] } },
    },
    { encodeValuesOnly: true }
  );
  const response = await axios.get(`/discounts/${id}?${query}`);
  return response?.data?.data;
};
