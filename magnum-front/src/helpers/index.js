console.log("ENV", process.env);
export default {
  getAbsolutePath: (path) => process.env.VUE_APP_API_URL + path,
};
