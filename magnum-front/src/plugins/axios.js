import axios from "axios";
console.log(process.env);
export default axios.create({
  baseURL: `${process.env.VUE_APP_API_URL}/api`,
  timeout: 10000,
});
