import Vue from "vue";
import App from "./App.vue";
// import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import VueMobileDetection from "vue-mobile-detection";
import "./assets/sass/style.sass";
import DefaultLayout from "@/layouts/DefaultLayout.vue";
import getAwesomeSwiper from "@/plugins/vue-awesome-swiper";
import helpers from "@/helpers";
const plugin = {
  install() {
    Vue.helpers = helpers;
    Vue.prototype.$helpers = helpers;
  },
};
Vue.use(plugin);
Vue.use(getAwesomeSwiper);
Vue.use(VueMobileDetection);

Vue.component("default-layout", DefaultLayout);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
