import { getPromos } from "@/api";

export const mutationTypes = {
  loadAllStocksStart: "mutation/loadAllStocksStart",
  loadAllStocksSuccess: "mutation/loadAllStocksSuccess",
  loadAllStocksFailure: "mutation/loadAllStocksFailure",
};
export const actionTypes = {
  loadAllStocks: "action/loadAllStocks",
};

export const stockModule = {
  namespaced: true,
  state: () => ({
    data: null,
    selectedStock: null,
    isLoading: false,
    error: null,
  }),

  mutations: {
    [mutationTypes.loadAllStocksStart](state) {
      state.isLoading = true;
    },
    [mutationTypes.loadAllStocksSuccess](state, payload) {
      state.isLoading = false;
      state.error = null;
      state.data = payload;
    },
    [mutationTypes.loadAllStocksFailure](state, payload) {
      state.isLoading = false;
      state.error = payload;
    },
  },
  actions: {
    [actionTypes.loadAllStocks]({ commit }) {
      commit(mutationTypes.loadAllStocksStart);

      return new Promise((resolve) => {
        getPromos()
          .then((promos) => {
            commit(mutationTypes.loadAllStocksSuccess, promos);
            resolve(promos);
          })
          .catch((e) => {
            commit(mutationTypes.loadAllStocksFailure, e);
          });
      });
    },
  },
};
