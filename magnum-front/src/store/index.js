import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import { cityModule } from "./modules/city";
import { newsModule } from "./modules/news";
import { stockModule } from "./modules/stock";
import recipes from "@/api/recipes.js"
Vue.use(Vuex);

const vueLocalStorage = new VuexPersist({
  key: "city",
  storage: window.localStorage,
  reducer: (state) => ({
    cityModule: {
      selectedCity: state.cityModule.selectedCity,
    },
  }),
});

export const mutationTypes = {
  setIsOpenMenu: "mutation/setIsOpenMenu",
  setIsOpenSelectCity: "mutation/setIsOpenSelectCity",
};
export const actionTypes = {
  changeIsOpenMenu: "action/changeIsOpenMenu",
  changeIsOpenSelectCity: "action/changeIsOpenSelectCity",
};

export default new Vuex.Store({
  state: {
    isOpenMenu: false,
    isOpenSelectCity: false,
    recs: [],
    rec: null
  },
  mutations: {
    SET_RECS(state, recs){
      state.recs = recs
    },
    SET_REC(state, rec){
      state.rec = rec
    },
    [mutationTypes.setIsOpenMenu](state) {
      state.isOpenMenu = !state.isOpenMenu;
    },
    [mutationTypes.setIsOpenSelectCity](state) {
      state.isOpenSelectCity = !state.isOpenSelectCity;
    },
  },
  actions: {
    fetchRecs({ commit }){
      return recipes.getRecipes()
      .then((res) => {
        commit('SET_RECS', res.data.data)
      })
      .catch((error) => {
        throw(error)
      })
    },
    fetchRec({ commit }, id){
      const rec = this.state.recs.find(rec => rec.id == id)
      if (rec) {
        commit('SET_REC', rec)
      } else {
        return recipes.getRecipe(id)
          .then(response => {
            commit('SET_REC', response.data)
          })
          .catch(error => {
            throw error
          })
      }
    },
    [actionTypes.changeIsOpenMenu]({ commit }) {
      commit(mutationTypes.setIsOpenMenu);
    },
    [actionTypes.changeIsOpenSelectCity]({ commit }) {
      commit(mutationTypes.setIsOpenSelectCity);
    },
  },
  modules: {
    cityModule,
    newsModule,
    stockModule,
  },
  plugins: [vueLocalStorage.plugin],
});
