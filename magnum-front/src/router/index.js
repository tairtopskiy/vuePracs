import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);
const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/catalog",
    name: "catalog",
    component: () => import("@/views/products/PageCatalog"),
  },
  {
    path: "/cooking",
    name: "cooking",
    component: () => import("@/views/cooking/PageCooking"),
  },
  {
    path: "/articles/:id",
    name: "articleItem",
    component: () => import("@/views/cooking/FullArticle"),
  },
  {
    path: "/recipes",
    name: "recipes",
    component: () => import("@/views/recipes/PageRecipes"),
  },
  {
    path: "/recipes/:id",
    name: "recipeItem",
    props: true,
    component: () => import("@/views/recipes/FullRecipe"),
  },

  {
    path: "/news",
    name: "news",
    component: () => import("@/views/news/PageNews"),
  },
  {
    path: "/news/:id",
    name: "newsItem",
    component: () => import("@/views/news/FullNews"),
    props: true,
  },
  {
    path: "/own-production",
    name: "ownProduction",
    component: () => import("@/views/own-production/PageOwnProduction"),
    meta: {
      layout: "fullscreen-layout",
    },
  },
  {
    path: "/private-label",
    name: "privateLabel",
    component: () => import("@/views/private-label/PagePrivateLabel"),
    meta: {
      layout: "fullscreen-layout",
    },
  },
  {
    path: "/shops",
    name: "shops",
    component: () => import("@/views/shops/PageShops"),
  },
  {
    path: "/shops/:id",
    name: "shopItem",
    component: () => import("@/views/shops/FullShop"),
  },
  {
    path: "/search",
    name: "search",
    component: () => import("@/views/SearchResult"),
  },
  {
    path: "/stocks",
    name: "stocks",
    component: () => import("@/views/stocks/PageStocks"),
  },
  {
    path: "/stocks/:id",
    name: "stockItem",
    component: () => import("@/views/stocks/FullStocks"),
  },
  {
    path: "/stocks/:id/products",
    name: "stockProducts",
    component: () => import("@/views/stocks/PageStocksProducts"),
  },
  {
    path: "/products/:id",
    name: "fullProduct",
    component: () => import("@/views/products/FullProduct"),
  },
  {
    path: "/magnumclub",
    name: "MagnumClub",
    component: () => import("@/views/MagnumClub"),
  },
  {
    path: "/magnumgo",
    name: "MagnumGo",
    component: () => import("@/views/MagnumGo"),
  },
  {
    path: "/about",
    name: "About",
    component: () => import("@/views/About"),
  },
  {
    path: "/:username/api",
    name: "Username",
    component: () => import("@/views/About")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
